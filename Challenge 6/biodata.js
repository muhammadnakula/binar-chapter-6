// npm install express
// const { urlencoded } = require('express');
const express        = require('express');
const app            = express();
const { user_game }  = require('./models');

app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.set("view engine", "ejs");

// get all biodata
app.get("/user_game/biodata", (req, res) => {
    user_biodata.findAll().then((user_biodata) => {
        res.status(200).json(user_biodata);
    });
});

// get biodata by ID
app.get("/user_game/biodata/:id", (req, res) => {
    user_biodata.findOne({
        where: {id: req.params.id},
    }).then((user_biodata) => {
        res.status(200).json(user_biodata);
    });
});

// post a biodata
app.post("/user_game/biodata", (req, res) => {
    user_biodata.create({
        name    : req.body.name,
        age     : req.body.age,
        address : req.body.address,
        approved: req.body.approved,
    })
    .then((user_biodata) => {
        res.status(201).json(user_biodata)
    })
    .catch((err) => {
        res.status(422).json("can't create an user")
    })
});


// update a biodata
app.put("/user_game/biodata/:id", (req, res) => {
    user_biodata.update(
    {
        name    : req.body.name,
        age     : req.body.age,
        address : req.body.address,
        approved: req.body.approved,
    },
    {
        where: {id: req.params.id},
    }
    )
    .then((user_biodata) => {
        res.status(201).json(user_biodata)
    })
    .catch((err) => {
        res.status(422).json("can't update an user")
    })
});

// delete a biodata
app.delete("/user_game/biodata/:id", (req, res) => {
    user_biodata.destroy({
        where: {id: req.params.id},
    }).then((user_biodata) => {
        res.status(200).json({
            message: `an user with ID ${req.params.id} has been deleted`,
        })
    })
});

// turn on server
app.listen(3000, () => {
    console.log("running at 3000 biodata")
});