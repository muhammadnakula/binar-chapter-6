// npm install express
// const { urlencoded } = require('express');
const express        = require('express');
const app            = express();
const { user_game }  = require('./models');

app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.set("view engine", "ejs");

// get all history
app.get("/user_game/history", (req, res) => {
    user_history.findAll().then((user_history) => {
        res.status(200).json(user_history);
    });
});

// get history by ID
app.get("/user_game/history/:id", (req, res) => {
    user_history.findOne({
        where: {id: req.params.id},
    }).then((user_history) => {
        res.status(200).json(user_history);
    });
});

// post a history
app.post("/user_game/history", (req, res) => {
    user_history.create({
        name        : req.body.name,
        score       : req.body.score,
        achievement : req.body.achievement,
        approved    : req.body.approved,
    })
    .then((user_history) => {
        res.status(201).json(user_history)
    })
    .catch((err) => {
        res.status(422).json("can't create an user")
    })
});


// update a history
app.put("/user_game/history/:id", (req, res) => {
    user_history.update(
    {
        name        : req.body.name,
        score       : req.body.score,
        achievement : req.body.achievement,
        approved    : req.body.approved,
    },
    {
        where: {id: req.params.id},
    }
    )
    .then((user_history) => {
        res.status(201).json(user_history)
    })
    .catch((err) => {
        res.status(422).json("can't update an user")
    })
});

// delete a history
app.delete("/user_game/history/:id", (req, res) => {
    user_history.destroy({
        where: {id: req.params.id},
    }).then((user_history) => {
        res.status(200).json({
            messscore: `an user with ID ${req.params.id} has been deleted`,
        })
    })
});

// turn on server
app.listen(3000, () => {
    console.log("running at 3000 history")
});