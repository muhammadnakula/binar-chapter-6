-- USER GAME TABLE 1 --
CREATE TABLE user_game(id BIGSERIAL PRIMARY KEY, username VARCHAR(20) NOT NULL, password VARCHAR(20) NOT NULL, approved BOOLEAN NOT NULL DEFAULT FALSE);

INSERT INTO user_game(username, password, approved) VALUES('gon', 'password', TRUE);
INSERT INTO user_game(username, password, approved) VALUES('killua', 'katasandi', TRUE);
INSERT INTO user_game(username, password, approved) VALUES('nakula', 'katakunci', TRUE);

SELECT * FROM user_game;
SELECT * FROM user_game WHERE password= 'katakunci';

UPDATE user_game SET author = 'vegeta' WHERE id=3;

DELETE FROM user_game WHERE id = 3;


-- USER GAME BIODATA TABLE 2 --
CREATE TABLE user_game_biodata(id BIGSERIAL PRIMARY KEY, name VARCHAR(30) NOT NULL, age INT NOT NULL, address CHAR(50) NOT NULL, approved BOOLEAN NOT NULL DEFAULT FALSE);

INSERT INTO user_game_biodata(name, age, address, approved) VALUES('nakula', '17', 'surabaya', TRUE);
INSERT INTO user_game_biodata(name, age, address, approved) VALUES('sadewa', '15', 'bali', TRUE);
INSERT INTO user_game_biodata(name, age, address, approved) VALUES('tjatra', '13', 'solo', TRUE);

SELECT * FROM user_game_biodata;
SELECT * FROM user_game_biodata WHERE age= '17';

UPDATE user_game_biodata SET name = 'wahyudi' WHERE id=2;

DELETE FROM user_game WHERE id = 3;


-- USER GAME HISTORY TABLE 3 --
CREATE TABLE user_game_history(id BIGSERIAL PRIMARY KEY, name VARCHAR(30) NOT NULL, score INT NOT NULL, achievement TEXT NOT NULL, approved BOOLEAN NOT NULL DEFAULT FALSE);

INSERT INTO user_game_history(name, score, achievement, approved) VALUES('nakula', '9', 'finish on first line', TRUE);
INSERT INTO user_game_history(name, score, achievement, approved) VALUES('sadewa', '8', 'make sound from phone', TRUE);
INSERT INTO user_game_history(name, score, achievement, approved) VALUES('tjatra', '10', 'fastest human on earth', TRUE);

SELECT * FROM user_game_history;
SELECT * FROM user_game_history WHERE name = 'tjatra';

UPDATE user_game_history SET score = '100' WHERE id=1;

DELETE FROM user_game WHERE id = 2;