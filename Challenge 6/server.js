// npm install express
// const { urlencoded } = require('express');
const express        = require('express');
const app            = express();
const { user_game }  = require('./models');

app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.set("view engine", "ejs");

// get success message
// app.get("/user_game/create", (req, res) => {
//     res.render("create")
// });

// get all user
app.get("/user_game", (req, res) => {
    user_game.findAll().then((user_game) => {
        res.status(200).json(user_game);
        // res.render("index", {
        //     user_game,
        // })
    });
});

// get user by ID
app.get("/user_game/:id", (req, res) => {
    user_game.findOne({
        where: {id: req.params.id},
    }).then((user_game) => {
        res.status(200).json(user_game);
    });
});

// post an user
app.post("/user_game", (req, res) => {
    user_game.create({
        username: req.body.username,
        password: req.body.password,
        approved: req.body.approved,
    })
    .then((user_game) => {
        res.status(201).json(user_game)
        // res.send("success created user")
    })
    .catch((err) => {
        res.status(422).json("can't create an user")
    })
});

// update an user
app.put("/user_game/:id", (req, res) => {
    user_game.update(
    {
        username: req.body.username,
        password: req.body.password,
        approved: req.body.approved,
    },
    {
        where: {id: req.params.id},
    }
    )
    .then((user_game) => {
        res.status(201).json(user_game)
    })
    .catch((err) => {
        res.status(422).json("can't update an user")
    })
});

// delete an user
app.delete("/user_game/:id", (req, res) => {
    user_game.destroy({
        where: {id: req.params.id},
    }).then((user_game) => {
        res.status(200).json({
            message: `an user with ID ${req.params.id} has been deleted`,
        })
    })
});

// turn on server
app.listen(3000, () => {
    console.log("running at 3000 user")
});